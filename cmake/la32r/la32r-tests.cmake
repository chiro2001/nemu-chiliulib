# message(STATUS "X_ROOT = ${X_ROOT}; CONFIG_X_ISA = ${CONFIG_X_ISA}")
file(GLOB_RECURSE IMAGE_FILES
        "${X_ROOT}/tests/image/asm/${CONFIG_X_ISA}/*.s"
        "${X_ROOT}/tests/image/asm/${CONFIG_X_ISA}/*.S"
        "${X_ROOT}/tests/image/asm/${CONFIG_X_ISA}/*.asm"
        "${X_ROOT}/tests/image/c/**/*.c"
        "${X_ROOT}/tests/image/c/**/*.cpp")
foreach (IMAGE_FILE ${IMAGE_FILES})
    string(REGEX REPLACE ".*/(.*)\\..*" "\\1" IMAGE_NAME ${IMAGE_FILE})
    set(IMAGE_NAME ${IMAGE_NAME}-${CONFIG_X_ISA}-nemu)
    message(STATUS "Adding image: ${IMAGE_NAME} ${IMAGE_FILE}")
#    set(IMAGE_EXE ${IMAGE_NAME}${CMAKE_EXECUTABLE_SUFFIX})
    set(IMAGE_EXE ${IMAGE_NAME}.elf)
    set(IMAGE_BIN ${IMAGE_NAME}.bin)
    set(IMAGE_ALL ${IMAGE_NAME}.all.bin)
    set(IMAGE_ASM ${IMAGE_NAME}.s)
    set(IMAGE_DATA ${IMAGE_NAME}.data)
    string(REGEX MATCH "kernel.s$" IMAGE_IS_KERNEL "${IMAGE_FILE}")
    if (NOT "${IMAGE_IS_KERNEL}" STREQUAL "")
        # TODO: support linux kernel linking
        message(STATUS "setting kernel start file for ${IMAGE_FILE}")
        add_executable(${IMAGE_EXE} ${IMAGE_FILE} "${X_ROOT}/tests/image/start/${CONFIG_X_ISA}-kernel.s")
        # target_compile_options(${IMAGE_EXE} PRIVATE -nostdlib -static -nostdinc -fno-builtin)
        target_compile_options(${IMAGE_EXE} PRIVATE -Iinclude -nostdinc -nostdlib -D_KERNEL -fno-builtin -D__loongarch32 -DMEMSIZE=0x04000 -DCPU_COUNT_PER_US=1000)
        target_link_options(${IMAGE_EXE} PRIVATE -T "${X_ROOT}/scripts/kernel-linker.ld" -e _start)
    else ()
        add_executable(${IMAGE_EXE} ${IMAGE_FILE})
        # target_compile_options(${IMAGE_EXE} PRIVATE -nostdlib -static -nostdinc -fno-builtin)
        string(REGEX MATCH "\\.s$" IMAGE_IS_ASM "${IMAGE_FILE}")
        if ("${IMAGE_IS_ASM}" STREQUAL "")
            target_link_options(${IMAGE_EXE} PRIVATE -L "${X_ROOT}/tests/system_newlib/" -T "${X_ROOT}/tests/system_newlib/pmon.ld" -lc -lm -lg -lpmon -lgcc -nostdlib -static -nostdinc -fno-builtin)
            message(STATUS "setting start file for C ${IMAGE_FILE}")
        else ()
            target_link_options(${IMAGE_EXE} PRIVATE -T "${X_ROOT}/scripts/test-image-linker.ld" -nostdlib -static -nostdinc -fno-builtin)
            message(STATUS "setting start file for ASM ${IMAGE_FILE}")
        endif ()
    endif ()
    add_custom_command(TARGET ${IMAGE_EXE}
            POST_BUILD
            COMMAND ${CMAKE_OBJCOPY} -O binary -j .start -j .main ${CMAKE_BINARY_DIR}/${IMAGE_EXE} ${CMAKE_BINARY_DIR}/${IMAGE_BIN} COMMAND_EXPAND_LISTS)
    add_custom_command(TARGET ${IMAGE_EXE}
            POST_BUILD
            COMMAND ${CMAKE_OBJCOPY} -O binary -j .rodata -j .data -j .sdata -j .init_array -j .got ${CMAKE_BINARY_DIR}/${IMAGE_EXE} ${CMAKE_BINARY_DIR}/${IMAGE_DATA} COMMAND_EXPAND_LISTS)
    add_custom_command(TARGET ${IMAGE_EXE}
            POST_BUILD
            COMMAND ${CMAKE_OBJCOPY} -O binary ${CMAKE_BINARY_DIR}/${IMAGE_EXE} ${CMAKE_BINARY_DIR}/${IMAGE_ALL} COMMAND_EXPAND_LISTS)
    add_custom_command(TARGET ${IMAGE_EXE}
            POST_BUILD
            COMMAND ${CMAKE_OBJDUMP} -d ${CMAKE_BINARY_DIR}/${IMAGE_EXE} > ${CMAKE_BINARY_DIR}/${IMAGE_ASM} COMMAND_EXPAND_LISTS)
    install(TARGETS ${IMAGE_EXE} DESTINATION .)
    install(FILES ${CMAKE_BINARY_DIR}/${IMAGE_BIN} DESTINATION .)
    install(FILES ${CMAKE_BINARY_DIR}/${IMAGE_ALL} DESTINATION .)
    install(FILES ${CMAKE_BINARY_DIR}/${IMAGE_ASM} DESTINATION .)
endforeach ()