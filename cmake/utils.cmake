# 工具函数

# 添加测试：
# 1. 名称中含有 `nemu-ref` 的测试会添加 nemu-ref 库作为依赖，同时设置 CTest 的命令行来包括 Difftest
# 2. 名称中含有 `nemu` 的测试会添加 nemu 作为依赖
# 3. 名称中含有 `verilator` 的测试会添加 verilator 库和 include 目录，需要编辑器代码高亮请先编译一次，来生成对应的文件
# 4. 名称中含有 `verilator` 和 `model` 的测试会添加 verilator 并且生成两个 target：
#       1. `model-测试名`：指定宏 USE_MODEL=1; DIFFTEST_测试名
#       2. `测试名`：无特殊指定，和 verilator 一致
# 5. 添加的 targets 写入 ${TARGETS_LIST}
function(my_add_tests TARGETS_LIST CONFIG_X_DIFFTEST)
    file(GLOB_RECURSE LIB_ASAN_PATH "/usr/lib/gcc/**/libasan.so")
    set(TARGETS_LIST_TEMP ${${TARGETS_LIST}})
    set(TEST_RUNS_TEMP)
    foreach (TEST_FILE ${TEST_FILES})
        # message("Test File: ${TEST_FILE}")
        string(REGEX REPLACE ".+/(.+)\\..*" "\\1" TEST_RUN ${TEST_FILE})
        string(REGEX MATCH "nemu" REGEX_MATCH_OUT ${TEST_RUN})
        if (NOT ${REGEX_MATCH_OUT} STREQUAL "")
            # message(STATUS "add test (nemu): add_executable(${TEST_RUN} ${TEST_FILE} ${SOURCE_FILES} ${X_SOURCE_FILES})")
            add_executable(${TEST_RUN} ${TEST_FILE} ${SOURCE_FILES} ${X_SOURCE_FILES})
            add_dependencies(${TEST_RUN} nemu)
            string(REGEX MATCH "nemu-ref" REGEX_MATCH_OUT ${TEST_RUN})
            if (NOT ${REGEX_MATCH_OUT} STREQUAL "")
                add_dependencies(${TEST_RUN} nemu-ref)
                add_test(NAME ${TEST_RUN} COMMAND ${TEST_RUN} -b ${DIFF_SO_CMD} ${PROJECT_BINARY_DIR}/${CONFIG_TEST_TARGET} COMMAND_EXPAND_LISTS)
            else ()
                add_test(NAME ${TEST_RUN} COMMAND ${TEST_RUN} -b ${PROJECT_BINARY_DIR}/${CONFIG_TEST_TARGET} COMMAND_EXPAND_LISTS)
            endif ()
            if (NOT "${CONFIG_CC_ASAN}" STREQUAL "" AND NOT WIN32)
                set_tests_properties(${TEST_RUN}
                        PROPERTIES ENVIRONMENT "LD_PRELOAD=${LIB_ASAN_PATH}")
            endif ()
            add_dependencies(${TEST_RUN} ddr_static)
            target_link_libraries(${TEST_RUN} ddr_static)
        else ()
            string(REGEX MATCH "shared" REGEX_MATCH_OUT ${TEST_FILE})
            if (NOT ${REGEX_MATCH_OUT} STREQUAL "")
                add_library(${TEST_RUN} SHARED ${TEST_FILE})
                install(TARGETS ${TEST_RUN} DESTINATION lib)
                install(TARGETS ${TEST_RUN} DESTINATION .)
            else ()
                # message(STATUS "add simple test: add_executable(${TEST_RUN} ${TEST_FILE} ${X_SOURCE_FILES})")
                add_executable(${TEST_RUN} ${TEST_FILE} ${X_SOURCE_FILES})
                add_test(NAME ${TEST_RUN} COMMAND ${TEST_RUN})
                if (NOT "${CONFIG_CC_ASAN}" STREQUAL "" AND NOT WIN32)
                    set_tests_properties(${TEST_RUN}
                            PROPERTIES ENVIRONMENT "LD_PRELOAD=${LIB_ASAN_PATH}")
                endif ()
            endif ()
            string(REGEX MATCH "verilator" REGEX_MATCH_OUT ${TEST_RUN})
            string(REGEX REPLACE "-" "_" TEST_RUN_PREFIX ${TEST_RUN})
            if (NOT ${REGEX_MATCH_OUT} STREQUAL "")
                # Find verilog sources
                set(VERILATOR_OUT_DIR ${PROJECT_BINARY_DIR}/verilator)
                file(MAKE_DIRECTORY ${VERILATOR_OUT_DIR})
                set(VERILATOR_SOURCE_DIR ${PROJECT_SOURCE_DIR}/tests/${TEST_RUN})
                file(GLOB_RECURSE VERILATOR_SOURCES "${VERILATOR_SOURCE_DIR}/*.v")
                message(STATUS "Verilator target: ${TEST_RUN} -> ${VERILATOR_OUT_DIR}/${TEST_RUN} from ${VERILATOR_SOURCES}")
                verilate(${TEST_RUN}
                        INCLUDE_DIRS "${PROJECT_SOURCE_DIR}/tests" "${VERILATOR_SOURCE_DIR}"
                        SOURCES ${VERILATOR_SOURCES}
                        PREFIX ${TEST_RUN_PREFIX}
                        TOP_MODULE ${TEST_RUN_PREFIX}
                            DIRECTORY ${VERILATOR_OUT_DIR}/${TEST_RUN_PREFIX})
                # Add model
                string(REGEX MATCH "model" REGEX_MATCH_OUT ${TEST_RUN})
                if (NOT ${REGEX_MATCH_OUT} STREQUAL "")
                    set(MODEL_TEST_RUN model-${TEST_RUN})
                    add_executable(${MODEL_TEST_RUN} ${TEST_FILE} ${X_SOURCE_FILES})
                    if (DEFINED CHISEL_X)
                        target_compile_definitions(${MODEL_TEST_RUN} PRIVATE CONFIG_CHISEL_X=1)
                    endif ()
                    if (${CONFIG_X_DIFFTEST})
                        message(STATUS "Difftest Enable for ${TEST_RUN} : ${CONFIG_X_DIFFTEST}")
                        target_compile_definitions(${MODEL_TEST_RUN} PRIVATE USE_MODEL=1)
                        target_compile_definitions(${MODEL_TEST_RUN} PRIVATE DIFFTEST_${TEST_RUN_PREFIX}=1)
                    else ()
                        message(STATUS "Difftest Disable for ${TEST_RUN} : ${CONFIG_X_DIFFTEST}")
                    endif ()
                    add_test(NAME ${MODEL_TEST_RUN} COMMAND ${MODEL_TEST_RUN})
                    verilate(${MODEL_TEST_RUN}
                            INCLUDE_DIRS "${PROJECT_SOURCE_DIR}/tests" "${VERILATOR_SOURCE_DIR}"
                            SOURCES ${VERILATOR_SOURCES}
                            PREFIX ${TEST_RUN_PREFIX}
                            TOP_MODULE ${TEST_RUN_PREFIX}
                                    DIRECTORY ${VERILATOR_OUT_DIR}/model_${TEST_RUN_PREFIX})
                    if (NOT "${CONFIG_CC_ASAN}" STREQUAL "" AND NOT WIN32)
                        set_tests_properties(${TEST_RUN}
                                PROPERTIES ENVIRONMENT "LD_PRELOAD=${LIB_ASAN_PATH}")
                    endif ()
                endif ()
            endif ()
        endif ()

        if (DEFINED CHISEL_X)
            target_compile_definitions(${TEST_RUN} PRIVATE CONFIG_CHISEL_X=1)
        endif ()

        list(APPEND TEST_RUNS_TEMP ${TEST_RUN})
        list(APPEND TARGETS_LIST_TEMP ${TEST_RUN})
    endforeach ()
    message(STATUS "Tests: ${TEST_RUNS_TEMP}")
    set(TEST_RUNS ${TEST_RUNS_TEMP} PARENT_SCOPE)
    set(${TARGETS_LIST} ${TARGETS_LIST_TEMP} PARENT_SCOPE)
endfunction()

# 添加库
# 被添加的库会增加定义：CONFIG_COMPILE_(大写文件名)
# 添加的 targets 写入 ${TARGETS_LIST}
function(my_add_libraries TARGETS_LIST)
    set(TARGETS_LIST_TEMP ${${TARGETS_LIST}})
    set(LIBRARY_RUNS_TEMP)
    foreach (LIBRARY_FILE ${LIBRARY_FILES})
        string(REGEX REPLACE ".+/(.+)\\..*" "\\1" LIBRARY_RUN ${LIBRARY_FILE})
        string(REGEX REPLACE "-" "_" DEFINITIONS ${LIBRARY_RUN})
        string(TOUPPER ${DEFINITIONS} DEFINITIONS)
        set(DEFINITIONS CONFIG_COMPILE_${DEFINITIONS})
        # message("Def: ${DEFINITIONS}")
        set(LIBRARY_RUN_STATIC ${LIBRARY_RUN}_static)
        add_library(${LIBRARY_RUN} SHARED ${LIBRARY_FILE} ${SOURCE_FILES} ${X_SOURCE_FILES})
        add_library(${LIBRARY_RUN_STATIC} STATIC ${LIBRARY_FILE} ${SOURCE_FILES} ${X_SOURCE_FILES})
        target_link_libraries(${LIBRARY_RUN} ${LIBRARY_RUN_STATIC})
        foreach (DEFINITION ${DEFINITIONS})
            target_compile_definitions(${LIBRARY_RUN} PRIVATE ${DEFINITION}=1)
            target_compile_definitions(${LIBRARY_RUN_STATIC} PRIVATE ${DEFINITION}=1)
        endforeach ()
        if (DEFINED CONFIG_ITRACE)
            target_link_libraries(${LIBRARY_RUN} ${LLVM_LIBS_A})
            target_link_libraries(${LIBRARY_RUN_STATIC} ${LLVM_LIBS_A})
        endif ()
        list(APPEND LIBRARY_RUNS_TEMP ${LIBRARY_RUN})
        list(APPEND TARGETS_LIST_TEMP ${LIBRARY_RUN})
        install(TARGETS ${LIBRARY_RUN} DESTINATION lib)
        install(TARGETS ${LIBRARY_RUN} DESTINATION .)
    endforeach ()
    message(STATUS "Libraries: ${LIBRARY_RUNS_TEMP}")
    set(LIBRARY_RUNS ${LIBRARY_RUNS_TEMP} PARENT_SCOPE)
    set(${TARGETS_LIST} ${TARGETS_LIST_TEMP} PARENT_SCOPE)
endfunction()

# 添加运行程序入口
function(my_add_programs TARGETS_LIST)
    set(TARGETS_LIST_TEMP ${${TARGETS_LIST}})
    set(PROGRAM_RUNS_TEMP)
    foreach (PROGRAM_FILE ${PROGRAM_FILES})
        string(REGEX REPLACE ".+/(.+)\\..*" "\\1" PROGRAM_RUN ${PROGRAM_FILE})
        add_executable(${PROGRAM_RUN} ${PROGRAM_FILE} ${SOURCE_FILES} ${X_SOURCE_FILES})
        if (DEFINED CONFIG_ITRACE)
            target_link_libraries(${PROGRAM_RUN} ${LLVM_LIBS_A})
        endif ()
        add_dependencies(${PROGRAM_RUN} ddr_static)
        target_link_libraries(${PROGRAM_RUN} ddr_static)
        list(APPEND PROGRAM_RUNS_TEMP ${PROGRAM_RUN})
        list(APPEND TARGETS_LIST_TEMP ${PROGRAM_RUN})
    endforeach ()
    message(STATUS "Programs: ${PROGRAM_RUNS_TEMP}")
    set(PROGRAM_RUNS ${PROGRAM_RUNS_TEMP} PARENT_SCOPE)
    set(${TARGETS_LIST} ${TARGETS_LIST_TEMP} PARENT_SCOPE)
endfunction()

function(compile_images)
    message(STATUS "Compiling test images")
    set(IMAGE_BINARY_DIR ${CMAKE_BINARY_DIR}/${CONFIG_X_ISA})
    set(IMAGE_SOURCE_DIR ${LITE_ROOT}/cmake/${CONFIG_X_ISA})
    set(TO_RUN_CMAKE
            -G${CMAKE_GENERATOR}
            -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
            -B ${IMAGE_BINARY_DIR}
            -S ${IMAGE_SOURCE_DIR}
            -DX_ROOT=${LITE_ROOT}
            -DCMAKE_TOOLCHAIN_FILE=${LITE_ROOT}/cmake/${CONFIG_X_ISA}/${CONFIG_X_ISA}.cmake
            -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}
            --no-warn-unused-cli
            )
    if (DEFINED ARGS_LIST)
        foreach (ARG ${ARGS_LIST})
            string(REGEX MATCH "ARGS_LIST" SEARCH_RESULT "${ARG}")
            if ("${SEARCH_RESULT}" STREQUAL "")
                list(APPEND TO_RUN_CMAKE "-D${ARG}=${${ARG}}")
            endif ()
        endforeach ()
    endif ()
    string(REGEX REPLACE ";" " " TO_RUN_CMAKE_STRING "${TO_RUN_CMAKE}")
    message(STATUS "cmake ${TO_RUN_CMAKE_STRING}")
    execute_process(COMMAND ${CMAKE_COMMAND} ${TO_RUN_CMAKE}
            WORKING_DIRECTORY ${IMAGE_SOURCE_DIR}
            RESULT_VARIABLE CONFIGURE_RESULT)
    if (NOT CONFIGURE_RESULT EQUAL 0)
        message(FATAL_ERROR "Failed when configuring image CMake")
    endif ()
    execute_process(COMMAND ${CMAKE_COMMAND} --build ${IMAGE_BINARY_DIR} -j
            WORKING_DIRECTORY ${IMAGE_SOURCE_DIR}
            RESULT_VARIABLE BUILD_RESULT)
    if (NOT BUILD_RESULT EQUAL 0)
        message(FATAL_ERROR "Failed when building image CMake")
    endif ()
    execute_process(COMMAND ${CMAKE_COMMAND} --install ${IMAGE_BINARY_DIR} --prefix=${CMAKE_BINARY_DIR}/images
            WORKING_DIRECTORY ${IMAGE_SOURCE_DIR}
            RESULT_VARIABLE INSTALL_RESULT)
    if (NOT INSTALL_RESULT EQUAL 0)
        message(FATAL_ERROR "Failed when installing image CMake")
    endif ()

endfunction()
