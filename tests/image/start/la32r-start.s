.section entry, "ax"
.globl _start
.type _start, @function

_start:
  pcaddu12i $ra, 0x0
  addi.w $ra, $ra, 0x14
  la $sp, _stack_pointer
  b main

_image_end:
  .word 0x80000000
