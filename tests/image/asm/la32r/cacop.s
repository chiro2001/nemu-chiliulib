.section entry, "ax"
.globl main
.type main, @function

main:
# 	li.w  $t0, 0x19
# 	csrwr $t0, 0x180
# 	li.w  $t0, 0xa0000009
# 	csrwr $t0, 0x181
# 	li.w  $t0, 0xb0
# 	csrwr $t0, 0x0
# 	li.w  $s8, 1
# 	la.local $s2, test_prog
# 	la.local $s3, test_prog_end
# 	addi.w $t0, $s2, 0x0
# 	li.w $t1, 0x00000000
# 	li.w $s4, 0x00000000
# 	pcaddu12i $s6, 0x0
# 	addi.w $s6, $s6, 8
# copy:
# 	ld.w $t7, $t0, 0x0
# 	st.w $t7, $t1, 0x0
# 	addi.w $t0, $t0, 0x4
# 	addi.w $t1, $t1, 0x4
# 	ble $t0, $s3, copy
# 	# ibar 0x0
# 	# dbar 0x0
# 	cacop 0, $zero, 0
# 	# jirl $zero, $zero, 0x0
# 	jirl $zero, $s4, 0
# 	li.w $r4, 0x0
# 	syscall 0x11
# 	# .align 8
# test_prog:
# 	addi.w $s0, $zero, 0x1
# 	addi.w $s1, $zero, 0x3
# 	addi.w $sp, $zero, 0x2
# 	addi.w $x, $zero, 0x3
# test_prog_loop:
# 	mul.w $s1, $s1, $sp
# 	add.w $s1, $s1, $s0
# 	addi.w $s0, $s0, 1
# 	blt $s0, $x, test_prog_loop
# # test_prog_end:
# 	pcaddu12i $s2, 0x0
# 	pcaddu12i $s3, 0x0
# 	addi.w $s2, $s2, -32
# 	addi.w $s3, $s3, (4*12)
# 	slli.w $t1, $s8, 12
# 	addi.w $s7, $zero, 3
# 	bge $s8, $s7, test_prog_finish
# 	addi.w $s8, $s8, 1
# 	add.w	$s4, $s2, $zero
# 	addi.w $t1, $s4, 0
# test_prog_end:
# 	jirl $zero, $s6, 0
# test_prog_finish:
# 	syscall 0x11
# 	b 0
	syscall 0x11
