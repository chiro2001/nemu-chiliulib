.section entry, "ax"
.globl main
.type main, @function

main:
    addi.w    $r1, $r0, 32
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    st.w  $r1, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    ld.w  $r2, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    st.w  $r1, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    ld.w  $r3, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    st.w  $r1, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    ld.w  $r4, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    st.w  $r1, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    ld.w  $r5, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    ld.w  $r1, $r0, 16
    ld.w  $r2, $r0, 16
    ld.w  $r3, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0

    addi.w    $r1, $r0, 32
    st.w  $r1, $r0, 16
    ld.w  $r2, $r0, 16
    st.w  $r1, $r0, 16
    ld.w  $r3, $r0, 16
    st.w  $r1, $r0, 16
    ld.w  $r4, $r0, 16
    st.w  $r1, $r0, 16
    ld.w  $r5, $r0, 16
    ld.w  $r1, $r0, 16
    ld.w  $r2, $r0, 16
    ld.w  $r3, $r0, 16
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
    addi.w    $r0, $r0, 0
  .word 0x80000000

