.section entry, "ax"
.globl main
.type main, @function

main:
  addi.w $r0, $r2, 1
  addi.w $r0, $r2, 2
  addi.w $r0, $r2, 3
  addi.w $r0, $r2, 7
  addi.w $r0, $r2, 14
  addi.w $r0, $r2, 28
  addi.w $r0, $r2, 56
  addi.w $r6, $r2, 1
  addi.w $r6, $r2, 2
  addi.w $r6, $r2, 3
  addi.w $r6, $r2, 7
  addi.w $r6, $r2, 14
  addi.w $r6, $r2, 28
  addi.w $r6, $r2, 56
  addi.w $r6, $r2, 133
  addi.w $r6, $r2, 258
  addi.w $r6, $r2, 511
  addi.w $r6, $r2, -1
  addi.w $r6, $r2, -3
  addi.w $r6, $r2, -9
  addi.w $r6, $r2, -98
  addi.w $r6, $r2, -231
  addi.w $r6, $r2, -510
  addi.w $r30, $r0, 1
  addi.w $r30, $r30, 2
  addi.w $r30, $r30, 3
  addi.w $r30, $r30, 7
  addi.w $r30, $r30, 14
  addi.w $r30, $r30, 28
  addi.w $r30, $r30, 56
  addi.w $r30, $r30, 133
  addi.w $r30, $r30, 258
  addi.w $r30, $r30, 511
  addi.w $r30, $r30, -1
  addi.w $r30, $r30, -3
  addi.w $r30, $r30, -9
  addi.w $r30, $r30, -98
  addi.w $r30, $r30, -231
  addi.w $r30, $r30, -510
  .word 0x80000000

