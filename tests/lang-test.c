#include <stdio.h>
#include <macro.h>

#define IS_DEFINED(x) IS_DEFINED2(x)
#define IS_DEFINED2(x) (#x[0] == 0 || (#x[0] >= '1' && #x[0] <= '9'))
#define TRACE(x, y) if (IS_DEFINED(x)) { std::cout << y << std::endl; }

#define __ARG_PLACEHOLDER_1 0,
#define __take_second_arg(__ignored, val, ...) val

#define __is_defined(x)         ___is_defined(x)
#define ___is_defined(val)      ____is_defined(__ARG_PLACEHOLDER_##val)
#define ____is_defined(arg1_or_junk)    __take_second_arg(arg1_or_junk 1, 0)

/**
 * 进行一些宏之类的测试。
 * @return
 */
int main() {
  puts("test");
#define THIS_DEFINED 1
#define THIS_DEFINED2 0
  IFDEF(THIS_DEFINED, puts("IFDEF OK"));
  MUXDEF(THIS_DEFINED, __KEEP, __IGNORE)(puts("IFDEF OK"));
  MUX_MACRO_PROPERTY(__P_DEF_, THIS_DEFINED, __KEEP, __IGNORE)(puts("IFDEF OK"));
  MUX_MACRO_PROPERTY(__P_DEF_, 1, __KEEP, __IGNORE)(puts("IFDEF OK"));
  MUX_WITH_COMMA(__P_DEF_1, __KEEP, __IGNORE)(puts("IFDEF OK"));
  CHOOSE2nd(__P_DEF_1 __KEEP, __IGNORE)(puts("IFDEF OK"));
  __KEEP(puts("KEEP OK"));
  if (IS_DEFINED(THIS_DEFINED)) puts("IS_DEFINED OK");
  if (!IS_DEFINED(THIS_DEFINED2)) puts("IS_DEFINED2 OK");
  puts("IS IFDEF OK ?");
  if (__is_defined(THIS_DEFINED)) puts("__is_defined OK");
  return 0;
}