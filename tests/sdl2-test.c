#define SDL_MAIN_HANDLED

#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  if (SDL_Init(SDL_INIT_VIDEO) == -1) {
    printf("Could not initialize SDL!\n");
    return 0;
  }
  printf("SDL initialized.\n");
  // SDL_Quit will force program exit, some libraries cannot free their memory.
  // SDL_Quit();
  return 0;
}
