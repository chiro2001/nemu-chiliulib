//
// Created by Chiro on 2022/2/18.
//

#include <common.h>

/**
 * TARGET nemu-test 用于测试 nemu 能不能正常运行。
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
  char *argv_test[] = {
          "nemu", "-b", NULL
  };
  return nemu_main(2, argv_test);
}