# NEMU-Lite

**NEMU-Lite**（适用于 LoongArch 的解释器与测试环境，**L**oongArch **I**nterpreter and **T**est **E**nvironment）是基于南大 NEMU 制作的一个简易 LoongArch 32 Reduced 模拟器。

NEMU-Lite 是 ChiselX 的一部分。

## Usage

```shell
# Configure, all necessary dependencies will be automatically downloaded and cached
cmake -B build -S . -G Ninja -D BATCH_MODE=ON
# Build, can build with threads
cmake --build build -j
# Install Binary files and libraries
cmake --install build --prefix build/install
```

```shell
# configure, build and install all files
make
# clean all
make clean
```

### Available command line args:

1. `BATCH_MODE`: Do not show GUI when building and testing, for example VGA SDL Window.
2. `CI_MODE`: Configure as testing on CI, will install all packages, enable `BATCH_MODE`, disable `CONFIG_ITRACE`

### Tested Compiler

1. GCC
2. MinGW-w64

### Dependences

1. `dlfcn-win32`
   1. dynamic linking library for MinGW-w64
   2. auto download from https://github.com/dlfcn-win32/dlfcn-win32
2. `llvm`
   1. used to disassemble instructions
   2. will not install automatically
   3. Installation
      1. *nix: install `llvm` package
      2. Windows: Download from releases: https://github.com/llvm/llvm-project/releases/tag/llvmorg-11.0.0
3. `python3`: used to execute scripts and generate config
   1. *nix: `python3`
   2. Windows: `python`
4. `kconfiglib`
   1. used to manage config of this project
   2. will auto install using pip: https://github.com/ulfalizer/Kconfiglib
5. `readline`: used on *nix only, manually run `sudo apt-get install libreadline-dev`
6. `SDL2`
   1. used to display as VGA device
   2. Windows: will auto download from https://www.libsdl.org/release/SDL2-devel-2.0.20-mingw.tar.gz
   3. *nix: manually run `sudo apt-get install libsdl2-dev`

## Notes

1. When using `Address Sanitizer` on linux, exter this first:
   ```shell
   export LD_PRELOAD=`find /usr/lib/gcc -name "libasan.so"`
   ```
   But do not add this to your `.bashrc`.

## Known problems

