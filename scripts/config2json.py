#!/usr/bin/python3

import sys
import json
import time

def config2json(kconfig: str, to: str):
    """Usage: config2json <path to .config> <path to .config.json>"""
    with open(kconfig, "r", encoding='utf8') as f:
        lines_raw = f.readlines()
    def replace_it(raw: str) -> str:
        splits = raw.split('=')
        return f"#define {splits[0]}" + (f" {splits[1]}" if splits[1] != 'y' else " 1")
    def get_expr(expr: str):
        if '"' in expr:
            return expr.replace('"', '')
        elif expr == 'y':
            return True
        else:
            try:
                return int(expr, 16 if expr.startswith('0x') else 10)
            except ValueError:
                return expr
    lines_json = [line.replace('\n', '').split('=') for line in lines_raw if not line.startswith('#') and len(line.replace('\n', '')) > 0]
    js = {}
    for line in lines_json:
        js[line[0]] = get_expr(line[1])
    with open(to, "w", encoding='utf8') as f:
        json.dump(js, f, indent=2)
    print(f"Wrote to {to}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(config2json.__doc__)
    else:
        config2json(sys.argv[1], sys.argv[2])
    time.sleep(1)
