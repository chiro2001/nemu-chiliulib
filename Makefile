all: install

config:
	cmake -B build -S . -G "Ninja"

config-release:
	cmake -B build -S . -G "Ninja" -DCMAKE_BUILD_TYPE=Release

build: config
	cmake --build build/

install: build
	cmake --install build --prefix build/install

release: config-release
	cmake --build build/
	cmake --install build --prefix build/install

clean:
	-rm -rf build/ cmake-debug-build/ cmake-release-build/