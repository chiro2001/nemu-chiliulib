#include <isa.h>
#include <memory/paddr.h>

void init_rand();

void init_log(const char *log_file);

void init_mem();

void init_device();

void init_sdb();

static void welcome() {
  Log("Build time: %s, %s", __TIME__, __DATE__);
  Log("Welcome to NEMU-Lite!");
}

#include <getopt.h>
#include <stdlib.h>

void sdb_set_batch_mode();

static char *log_file = NULL;

img_t *imgs = NULL;

static void load_all_imgs() {
  for (img_t *ptr = imgs; ptr != NULL; ptr = ptr->next) {
    load_img(ptr->file_name, ptr->address);
  }
}

int parse_args(int argc, char *argv[]) {
  const struct option table[] = {
          {"batch", no_argument,       NULL, 'b'},
          {"log",   required_argument, NULL, 'l'},
          {"help",  no_argument,       NULL, 'h'},
          {"image", required_argument, NULL, 'i'},
          {0,       0,                 NULL, 0},
  };
  int o;
  while ((o = getopt_long(argc, argv, "-bl:hi:", table, NULL)) != -1) {
    switch (o) {
      case 'b':
        sdb_set_batch_mode();
        break;
      case 'l':
        log_file = optarg;
        break;
      case 'i': {
        img_t *img = (img_t *) malloc (sizeof(img_t));
        sscanf(optarg, "0x%x:%s", &(img->address), img->file_name);
        img->next = imgs;
        imgs = img;
        break;
      }
      case 'h':
        printf("Usage: nemu [OPTION...]\n\n");
        printf("\t-b,--batch              run with batch mode\n");
        printf("\t-l,--log=FILE           output log to FILE\n");
        printf("\t-h,--help               print this help\n");
        printf("\t-i,--image=addr:file    load image to specified address\n");
        printf("\n");
        exit(0);
      default:
        printf("Unspecified argument `%c\'!\n", o);
        exit(-1);
    }
  }
  return 0;
}

static void sig_exit_nemu(int sig) {
  nemu_state.state = NEMU_ABORT;
  nemu_state.halt_pc = cpu.pc;
}

static void sig_print_status(int sig) {
  // TEST ONLY
//  ESTAT->is_2_12 |= 0x2;
  isa_reg_display();
}

#include <signal.h>

static inline void bind_signal() {
  signal(SIGTSTP, sig_exit_nemu);
  signal(SIGTERM, sig_exit_nemu);
  signal(SIGINT, sig_print_status);
}

void init_monitor(int argc, char *argv[]) {
  /* Bind signals */
  bind_signal();

  /* Parse arguments. */
  IFNDEF(CONFIG_COMPILE_NEMU_REF, parse_args(argc, argv));

  /* Set random seed. */
  init_rand();

  /* Open the log file. */
  init_log(log_file);

  /* Initialize memory. */
  init_mem();

  /* Initialize devices. */
  IFDEF(CONFIG_DEVICE, init_device());

  /* Perform ISA dependent initialization. */
  init_isa();

  /* Init sections info */
  IFDEF(CONFIG_FTRACE, elf_init());

  /* Load the image to memory. This will overwrite the built-in image. */
  load_all_imgs();

  /* Initialize the simple debugger. */
  init_sdb();

  /* Display welcome message. */
  welcome();
}