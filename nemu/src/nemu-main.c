/**
 * @file nemu-main.c
 * @brief NEMU-Lite entrance
 * @author Chiro, Hans
 */

#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <build_config.h>

void engine_start();
int is_exit_status_bad();
void mem_release();

/**
 * NEMU 运行主函数，但是不是入口函数
 * @param argc
 * @param argv
 * @return
 */
int nemu_main(int argc, char **argv) {
  /* Initialize the monitor. */
  init_monitor(argc, argv);

  /* Start engine. */
  engine_start();

  /* Release memory */
  mem_release();

  return is_exit_status_bad();
}

