#include "common.h"

/**
 * TARGET nemu 的入口函数，将进入 sdb
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char **argv) {
  printf("NEMU-Lite: LoongArch Interpreter and Testing Environment\n");
  printf("A part of ChiselX.\n");
  return nemu_main(argc, argv);
}