/**
 * Virtual Memory
 */

#include <isa.h>

#include <memory/paddr.h>
#include <memory/vaddr.h>


__attribute__((noinline))
static word_t vaddr_mmu_read(struct Decode *s, vaddr_t addr, int len, int type, paddr_t *paddr_ret) {
  paddr_t paddr = isa_mmu_translate(addr, len, type);
  if (paddr_ret != NULL) {
    *paddr_ret = paddr;
  }
  word_t rdata = paddr_read(paddr, len);
  return rdata;
}

__attribute__((noinline))
static void vaddr_mmu_write(struct Decode *s, vaddr_t addr, int len, word_t data, paddr_t *paddr_ret) {
  paddr_t paddr = isa_mmu_translate(addr, len, MEM_TYPE_WRITE);
  if (paddr_ret != NULL) {
    *paddr_ret = paddr;
  }
  paddr_write(paddr, len, data);
}


static inline word_t vaddr_read_internal(void *s, vaddr_t addr, int len, int type, int mmu_mode, paddr_t *paddr) {
  if (mmu_mode == MMU_DIRECT) { return paddr_read(addr, len); }
  return vaddr_mmu_read((struct Decode *)s, addr, len, type, paddr);
}

word_t vaddr_ifetch(vaddr_t addr, int len) {
  return vaddr_read_internal(NULL, addr, len, MEM_TYPE_IFETCH, isa_mmu_check(addr, len, MEM_TYPE_IFETCH), NULL);
}

word_t vaddr_read(struct Decode *s, vaddr_t addr, int len, int mmu_mode) {
#ifdef CONFIG_MTRACE
  word_t paddr;
  word_t buf = vaddr_read_internal(s, addr, len, MEM_TYPE_READ, mmu_mode, &paddr);
  printt(MTRACE, "Memory read at virtual address 0x%08x, physical address: 0x%08x, data: %08x, MMU mode: %d", addr, mmu_mode == 0 ? addr : paddr, buf, mmu_mode);
  return buf;
#else
  return vaddr_read_internal(s, addr, len, MEM_TYPE_READ, mmu_mode, NULL);
#endif
}

void vaddr_write(struct Decode *s, vaddr_t addr, int len, word_t data, int mmu_mode) {
  word_t paddr;
  if (mmu_mode == MMU_DIRECT) { paddr_write(addr, len, data); }
  else vaddr_mmu_write(s, addr, len, data, &paddr);
  printt(MTRACE, "Memory write at virtual address 0x%08x, physical address: 0x%08x, data: %08x, MMU mode: %d", addr, mmu_mode == 0 ? addr : paddr, data, mmu_mode);
}
