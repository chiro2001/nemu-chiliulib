#include "../local-include/intr.h"
#include <cpu/cpu.h>
#include <device/mmio.h>
#include <isa.h>
#include <memory/host.h>
#include <memory/paddr.h>
#include <stdlib.h>
#include <time.h>


#ifdef CONFIG_EXT_DYNAMIC_PMEM
uint8_t *pmem = NULL;
// for page align
static uint8_t *pmem_raw = NULL;
#else
static uint8_t pmem[CONFIG_MSIZE] PG_ALIGN = {};
#endif


// uint8_t* guest_to_host(paddr_t paddr) { return paddr - CONFIG_MBASE + pmem; }
// paddr_t host_to_guest(uint8_t *haddr) { return haddr + CONFIG_MBASE - pmem; }

// uint8_t* guest_to_host(paddr_t paddr) {
//   return pmem + paddr - CONFIG_MBASE;
// }

// paddr_t host_to_guest(uint8_t *haddr) {
//   return haddr + CONFIG_MBASE - pmem;
// }


// static inline word_t pmem_read(paddr_t addr, int len) {
// //  if (addr == 0x00003240) {
// //    uint32_t buf = host_read(guest_to_host(addr), len);
// //    printf("0x%08x\n", buf);
// //    return buf;
// //  }
//   return host_read(guest_to_host(addr), len);
// }

// extern uint64_t g_nr_guest_instr;

// static inline void pmem_write(paddr_t addr, int len, word_t data) {
//   // if (addr == 0x00003240) {printf("HEY! #%lu, PC = 0x%08x, data = 0x%08x\n", g_nr_guest_instr, cpu.pc, data); }
//   host_write(guest_to_host(addr), len, data);
// }

int g_init_mem_seed = -1;

void init_mem() {
#ifdef CONFIG_EXT_DYNAMIC_PMEM
  pmem_raw = malloc(CONFIG_MSIZE * sizeof(uint8_t) + 8192);
  Assert(pmem_raw, "Cannot dynamically create memory!");
  pmem = pmem_raw + 8192 - ((size_t) pmem_raw % 4096);
#endif
  Log("    pmem:\t[%p, %p] => [0x%08x, 0x%08x]", pmem, pmem + CONFIG_MSIZE * sizeof(uint32_t) - 1, CONFIG_MBASE, (CONFIG_MSIZE - 1) + CONFIG_MBASE);
#ifdef CONFIG_MEM_ZERO_INIT
  Log("Filling memory with zeros...");
  uint32_t *p = (uint32_t *) pmem;
  for (int i = 0; i < (int) (CONFIG_MSIZE / sizeof(p[0])); i++) {
    p[i] = 0;
  }
#endif
#ifdef CONFIG_MEM_RANDOM
  if (g_init_mem_seed == -1) {
#if CONFIG_MEM_RANDOM_SEED == -1
    int seed = time(0);
    Log("Filling memory with random values... SEED = %d", seed);
    srand(seed);
#else
    Log("Filling memory with random values... SEED = %d", CONFIG_MEM_RANDOM_SEED);
    srand(CONFIG_MEM_RANDOM_SEED);
#endif
  } else {
    Log("Filling memory with random values... SEED = %d", g_init_mem_seed);
    srand(g_init_mem_seed);
  }
  uint32_t *p = (uint32_t *) pmem;
  int i;
  for (i = 0; i < (int) (CONFIG_MSIZE / sizeof(p[0])); i++) {
    p[i] = rand();
  }
#endif
}

void release_mem() {
#ifdef CONFIG_EXT_DYNAMIC_PMEM
  if (pmem_raw) free(pmem_raw);
  pmem_raw = NULL;
  pmem = NULL;
#endif
}

/* Memory accessing interfaces */

word_t paddr_read(paddr_t addr, int len) {
  if (likely(in_pmem(addr))) {
    return pmem_read(addr, len);
  } else
    return mmio_read(addr, len);
}

void paddr_write(paddr_t addr, int len, word_t data) {
  if (likely(in_pmem(addr))) pmem_write(addr, len, data);
  // else mmio_write(addr, len, data);
  mmio_write(addr, len, data);
}

int load_img(const char *img_file, paddr_t address) {
  if (img_file == NULL) {
    Err("Invalid file name %s", img_file);
    return -1;
  }

  FILE *fp = fopen(img_file, "rb");
  if (!fp) {
    Err("Can not open `%s\'", img_file);
    return -1;
  }

  fseek(fp, 0, SEEK_END);
  long size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  int ret = (int) fread(guest_to_host(address), size, 1, fp);
  if (ret != 1) {
    Err("Can not load image `%s\' to address 0x%08x", img_file, address);
    return 1;
  }
  fclose(fp);

  Log("Successfully loaded image `%s\' to %p -> 0x%08x", img_file, guest_to_host(address), address);
  return 0;
}


/**
 * @brief 释放动态申请的内存
 */
void mem_release() {
#ifdef CONFIG_EXT_DYNAMIC_PMEM
  if (pmem_raw) free(pmem_raw);
  pmem = NULL;
#endif
}