/**
 * @file timer.c
 * @brief Timer for LA32r
 * @author Hans
*/

#include <isa.h>

#define FREQ_DIV 64

static uint32_t counter_cnt;
static uint32_t timer_cnt;

void timer_action() {
  counter_cnt++;
  if (counter_cnt == FREQ_DIV) {
    cpu.stable_counter.val++;
    counter_cnt = 0;
  }
  cpu.stable_counter.val++;
  if (TCFG->en) {
    if (TVAL->val == 0) {
      printt(TMTRACE, "Timer triggered");
      ESTAT->is_2_12 |= 0x200;
      if (TCFG->periodic == 1) {
        TVAL->val = (TCFG->initval << 2);
        printt(TMTRACE, "Timer was reset to %d", TVAL->val);
      } else {
        TCFG->en = 0;
        printt(TMTRACE, "Timer was disabled");
      }
    } else {
      timer_cnt++;
      if (timer_cnt == FREQ_DIV) {
        TVAL->val--;
        timer_cnt = 0;
      }
    }
  }
}