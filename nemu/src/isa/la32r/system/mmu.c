/***************************************************************************************
* Copyright (c) 2021-2022 Weitong Wang, University of Chinese Academy of Sciences
*
* NEMU is licensed under Mulan PSL v2.
* You can use this software according to the terms and conditions of the Mulan PSL v2.
* You may obtain a copy of Mulan PSL v2 at:
*          http://license.coscl.org.cn/MulanPSL2
*
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
* EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
* MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
*
* See the Mulan PSL v2 for more details.
***************************************************************************************/

#include <stdlib.h>
#include <isa.h>
#include <cpu/cpu.h>
#include <memory/paddr.h>
#include <memory/vaddr.h>
#include "../local-include/csr.h"
#include "../local-include/intr.h"
#include "../local-include/mmu.h"

struct tlb_struct JTLB[CONFIG_JTLB_ENTRIES];

void init_mmu() {
  int i;
  for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
    JTLB[i].hi.E = JTLB[i].lo[0].V = JTLB[i].lo[1].V = 0;
  }
}

static inline void update_tlb(uint32_t idx) {
  JTLB[idx].hi.PS = TLBIDX->ps;
  JTLB[idx].hi.VPPN = TLBEHI->vppn;
  JTLB[idx].hi.ASID = ASID->asid;

  JTLB[idx].lo[0].V = TLBELO0->v;
  JTLB[idx].lo[0].D = TLBELO0->d;
  JTLB[idx].lo[0].PLV = TLBELO0->plv;
  JTLB[idx].lo[0].MAT = TLBELO0->mat;
  JTLB[idx].lo[0].PPN = TLBELO0->ppn;

  JTLB[idx].lo[1].V = TLBELO1->v;
  JTLB[idx].lo[1].D = TLBELO1->d;
  JTLB[idx].lo[1].PLV = TLBELO1->plv;
  JTLB[idx].lo[1].MAT = TLBELO1->mat;
  JTLB[idx].lo[1].PPN = TLBELO1->ppn;

  JTLB[idx].hi.G = TLBELO0->g & TLBELO1->g;
  if (ESTAT->ecode != 0x3f) {
    JTLB[idx].hi.E = ~(TLBIDX->ne);
  } else {
    JTLB[idx].hi.E = 1;
  }
}

void tlbwr() {
  update_tlb(TLBIDX->index);
}

void tlbfill(uint32_t idx) {
  update_tlb(idx);
}

int tlbsrch() {
  int i = 0;
  int find = 0;
  for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
    if (JTLB[i].hi.E) {
      if ((JTLB[i].hi.G == 1) || (ASID->asid == JTLB[i].hi.ASID)) {
        if (JTLB[i].hi.PS == 12) {
          if (TLBEHI->vppn == JTLB[i].hi.VPPN) {
            TLBIDX->index = i;
            TLBIDX->ne = 0;
            find++;
          }
        } else if (JTLB[i].hi.PS == 21) {
          if ((TLBEHI->vppn >> 9) == (JTLB[i].hi.VPPN >> 9)) {
            TLBIDX->index = i;
            TLBIDX->ne = 0;
            find++;
          }
        }
      }
    }
  }
  if (find == 0)
    TLBIDX->ne = 1;
  if (find > 1) {
    Err("Multiple JTLB entries searched, please check JTLB\n");
    nemu_state.state = NEMU_ABORT;
  }

  return find;
}

void tlbrd() {
  if (JTLB[TLBIDX->index].hi.E) {
    TLBEHI->vppn = JTLB[TLBIDX->index].hi.VPPN;
    TLBIDX->ps = JTLB[TLBIDX->index].hi.PS;
    ASID->asid = JTLB[TLBIDX->index].hi.ASID;

    TLBELO0->g = TLBELO1->g = JTLB[TLBIDX->index].hi.G;
    TLBELO0->v = JTLB[TLBIDX->index].lo[0].V;
    TLBELO0->d = JTLB[TLBIDX->index].lo[0].D;
    TLBELO0->plv = JTLB[TLBIDX->index].lo[0].PLV;
    TLBELO0->mat = JTLB[TLBIDX->index].lo[0].MAT;
    TLBELO0->ppn = JTLB[TLBIDX->index].lo[0].PPN;

    TLBELO1->v = JTLB[TLBIDX->index].lo[1].V;
    TLBELO1->d = JTLB[TLBIDX->index].lo[1].D;
    TLBELO1->plv = JTLB[TLBIDX->index].lo[1].PLV;
    TLBELO1->mat = JTLB[TLBIDX->index].lo[1].MAT;
    TLBELO1->ppn = JTLB[TLBIDX->index].lo[1].PPN;
  } else {
    TLBEHI->vppn = 0;//JTLB[TLBIDX->index].hi.VPPN;
    TLBIDX->ps = 0;//JTLB[TLBIDX->index].hi.PS;
    ASID->asid = 0;//JTLB[TLBIDX->index].hi.ASID;

    TLBELO0->g = 0;//TLBELO1->g = JTLB[TLBIDX->index].hi.G;
    TLBELO0->v = 0;//JTLB[TLBIDX->index].lo[0].V;
    TLBELO0->d = 0;//JTLB[TLBIDX->index].lo[0].D;
    TLBELO0->plv = 0;//JTLB[TLBIDX->index].lo[0].PLV;
    TLBELO0->mat = 0;//JTLB[TLBIDX->index].lo[0].MAT;
    TLBELO0->ppn = 0;//JTLB[TLBIDX->index].lo[0].PPN;

    TLBELO1->g = 0;
    TLBELO1->v = 0;//JTLB[TLBIDX->index].lo[1].V;
    TLBELO1->d = 0;//JTLB[TLBIDX->index].lo[1].D;
    TLBELO1->plv = 0;//JTLB[TLBIDX->index].lo[1].PLV;
    TLBELO1->mat = 0;//JTLB[TLBIDX->index].lo[1].MAT;
    TLBELO1->ppn = 0;//JTLB[TLBIDX->index].lo[1].PPN;
  }
  TLBIDX->ne = ~(JTLB[TLBIDX->index].hi.E);
}

void invtlb(uint32_t op, uint32_t asid, uint32_t va) {
  int i = 0;
  switch (op) {
    case 0x0:
    case 0x1:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        JTLB[i].hi.E = 0;
      }
      break;
    case 0x2:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if (JTLB[i].hi.G == 1)
          JTLB[i].hi.E = 0;
      }
      break;
    case 0x3:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if (JTLB[i].hi.G == 0)
          JTLB[i].hi.E = 0;
      }
      break;
    case 0x4:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if ((JTLB[i].hi.G == 0) && (JTLB[i].hi.ASID == asid))
          JTLB[i].hi.E = 0;
      }
      break;
    case 0x5:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if ((JTLB[i].hi.G == 0) && (JTLB[i].hi.ASID == asid)) {
          if (JTLB[i].hi.PS == 12) {
            if (JTLB[i].hi.VPPN == (va >> 13)) {
              JTLB[i].hi.E = 0;
            }
          } else if (JTLB[i].hi.PS == 21) {
            if ((JTLB[i].hi.VPPN >> 9) == (va >> 22)) {
              JTLB[i].hi.E = 0;
            }
          }
        }
      }
      break;
    case 0x6:
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if ((JTLB[i].hi.G == 1) || JTLB[i].hi.ASID == asid) {
          if (JTLB[i].hi.PS == 12) {
            if (JTLB[i].hi.VPPN == (va >> 13)) {
              JTLB[i].hi.E = 0;
            }
          } else if (JTLB[i].hi.PS == 21) {
            if ((JTLB[i].hi.VPPN >> 9) == (va >> 22)) {
              JTLB[i].hi.E = 0;
            }
          }
        }
      }
      break;
    default:
      Err("Unknown OP 0x%x for INVTLB, EX_INE will be raised", op);
      exception(EX_INE);
      break;
  }
}

// va -> pa
paddr_t isa_mmu_translate(vaddr_t vaddr, int len, int type) {
  int match_dmw0 = 0;
  int match_dmw1 = 0;
  int match_tlb = 0;
  int matched_tlb_index = 0;
  int even_or_odd = 0; // 1:chose odd page1  0:chose even page0
  if (isa_mmu_state() == MMU_DIRECT) {
    return vaddr;
  } else if (isa_mmu_state() == MMU_TRANSLATE) {
    if ((((DMW0->plv0 == 1) && (CRMD->plv == 0)) || ((DMW0->plv3 == 1) && (CRMD->plv == 3))) &&
        ((vaddr >> 29) == DMW0->vseg))
      match_dmw0 = 1;
    if ((((DMW1->plv0 == 1) && (CRMD->plv == 0)) || ((DMW1->plv3 == 1) && (CRMD->plv == 3))) &&
        ((vaddr >> 29) == DMW1->vseg))
      match_dmw1 = 1;

    if (match_dmw0 & match_dmw1) {
      Err("Both DMW reg matched, should be a fatal error");
      nemu_state.state = NEMU_ABORT;
    } else if (match_dmw0) {
      return (((DMW0->pseg) << 29) | (vaddr & 0x1fffffff));
    } else if (match_dmw1) {
      return (((DMW1->pseg) << 29) | (vaddr & 0x1fffffff));
    } else {     // DMW reg not matched, use JTLB

      if ((CRMD->plv == 0x3) && (vaddr & ((vaddr_t) 0x80000000))) {
        printt(MMUTRACE, "Illegal memory access to 0x%08x under PLV3 at PC %08x", vaddr, cpu.pc);
        if (type == MEM_TYPE_IFETCH) {
          ESTAT->esubcode = 0;
          BADV->val = cpu.pc; // CPU.PC is the same as vaddr here..
        } else {
          ESTAT->esubcode = 1;
          BADV->val = vaddr;
        }
        exception(EX_ADE);
      }

      int i = 0;
      for (i = 0; i < CONFIG_JTLB_ENTRIES; i++) {
        if ((JTLB[i].hi.E == 1) && ((JTLB[i].hi.G == 1) || (JTLB[i].hi.ASID == ASID->asid))) {
          if (JTLB[i].hi.PS == 12) {
            if ((vaddr >> 13) == JTLB[i].hi.VPPN) {
              matched_tlb_index = i;
              even_or_odd = (vaddr >> 12) % 2;
              match_tlb++;
            }
          } else if (JTLB[i].hi.PS == 21) {
            if ((vaddr >> 22) == (JTLB[i].hi.VPPN >> 9)) {
              matched_tlb_index = i;
              even_or_odd = (vaddr >> 21) % 2;
              match_tlb++;
            }
          }
        }
      }
      if (match_tlb == 0) {
        // printf("PC: 0x%x [NEMU]: EXCEPTION TLBR\n",cpu.pc);
        BADV->val = vaddr;
        TLBEHI->vppn = vaddr >> 13;
        exception(EX_TLBR);
      } else if (match_tlb > 1) {
        Err("Multiple JTLB entries matched");
        nemu_state.state = NEMU_ABORT;
      } else if (match_tlb == 1) {
        if (JTLB[matched_tlb_index].lo[even_or_odd].V == 0) {
          if (type == MEM_TYPE_IFETCH) {
            // printf("PC: 0x%x [NEMU]: EXCEPTION PIF\n",cpu.pc);
            BADV->val = vaddr;
            TLBEHI->vppn = vaddr >> 13;
            exception(EX_PIF);
          } else if (type == MEM_TYPE_READ) {
            // printf("PC: 0x%x [NEMU]: EXCEPTION PIL\n",cpu.pc);
            BADV->val = vaddr;
            TLBEHI->vppn = vaddr >> 13;
            exception(EX_PIL);
          } else if (type == MEM_TYPE_WRITE) {
            // printf("PC: 0x%x [NEMU]: EXCEPTION PIS\n",cpu.pc);
            BADV->val = vaddr;
            TLBEHI->vppn = vaddr >> 13;
            exception(EX_PIS);
          }
        }
        if (JTLB[matched_tlb_index].lo[even_or_odd].PLV < CRMD->plv) {
          // printf("PC: 0x%x [NEMU]: EXCEPTION PPI\n",cpu.pc);
          BADV->val = vaddr;
          TLBEHI->vppn = vaddr >> 13;
          exception(EX_PPI);
        }
        if ((type == MEM_TYPE_WRITE) && (JTLB[matched_tlb_index].lo[even_or_odd].D == 0)) {
          // printf("PC: 0x%x [NEMU]: EXCEPTION PME\n",cpu.pc);
          BADV->val = vaddr;
          TLBEHI->vppn = vaddr >> 13;
          exception(EX_PME);
        }

        if (JTLB[matched_tlb_index].hi.PS == 12) {
          if (false
              IFDEF(CONFIG_MMUTRACE_IFETCH, || type == MEM_TYPE_IFETCH)
              IFDEF(CONFIG_MMUTRACE_READ, || type == MEM_TYPE_READ)
              IFDEF(CONFIG_MMUTRACE_WRITE, || type == MEM_TYPE_WRITE)
              ) {
            printt(MMUTRACE, "%08x => %08x", vaddr,
                   (((JTLB[matched_tlb_index].lo[even_or_odd].PPN) << 12) | (vaddr & 0x00000fff)));
          }
          return (((JTLB[matched_tlb_index].lo[even_or_odd].PPN) << 12) | (vaddr & 0x00000fff));
        } else if (JTLB[matched_tlb_index].hi.PS == 21) {
          if (false
              IFDEF(CONFIG_MMUTRACE_IFETCH, || type == MEM_TYPE_IFETCH)
              IFDEF(CONFIG_MMUTRACE_READ, || type == MEM_TYPE_READ)
              IFDEF(CONFIG_MMUTRACE_WRITE, || type == MEM_TYPE_WRITE)
              ) {
            printt(MMUTRACE, "%08x => %08x", vaddr,
                   ((((JTLB[matched_tlb_index].lo[even_or_odd].PPN) << 12) & 0xffe00000) | (vaddr & 0x001fffff)));
          }
          return ((((JTLB[matched_tlb_index].lo[even_or_odd].PPN) << 12) & 0xffe00000) | (vaddr & 0x001fffff));
        } else {
          Err("Invalid PS %d in JTLB entry #%d", JTLB[matched_tlb_index].hi.PS, matched_tlb_index);
          nemu_state.state = NEMU_ABORT;
        }
      }
    }
  }
  return vaddr;
}

void print_tlb_entry(int n) {
  printf("JTLB[%d]:\n", n);
  printf("HI:0x%016lx\n", JTLB[n].hi.val);
  printf("VPPN: 0x%x  PS: 0x%x  G: 0x%x  ASID: 0x%x  E: 0x%x\n", JTLB[n].hi.VPPN, JTLB[n].hi.PS, JTLB[n].hi.G,
         JTLB[n].hi.ASID, JTLB[n].hi.E);
  printf("LO[0]:0x%08x\n", JTLB[n].lo[0].val);
  printf("PPN0: 0x%x  PLV0: 0x%x  MAT0: 0x%x  D0: 0x%x  V0: 0x%x\n", JTLB[n].lo[0].PPN, JTLB[n].lo[0].PLV,
         JTLB[n].lo[0].MAT, JTLB[n].lo[0].D, JTLB[n].lo[0].V);
  printf("LO[1]:0x%08x\n", JTLB[n].lo[1].val);
  printf("PPN1: 0x%x  PLV1: 0x%x  MAT1: 0x%x  D1: 0x%x  V1: 0x%x\n", JTLB[n].lo[1].PPN, JTLB[n].lo[1].PLV,
         JTLB[n].lo[1].MAT, JTLB[n].lo[1].D, JTLB[n].lo[1].V);
}