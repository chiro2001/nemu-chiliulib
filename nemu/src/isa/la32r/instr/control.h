static inline void trace_jump(vaddr_t target, char *inst) {
#if defined(CONFIG_JTRACE) || defined(CONFIG_FTRACE)
  const char *found_section = find_section(target);
  if (!(!found_section || (found_section && *found_section == '-'))) {
#ifdef CONFIG_FTRACE
    if (MUXDEF(CONFIG_EXT_FTRACE_FULL, 1, 0) || *found_section == '<') printt(FTRACE, "JTRACE Jump to %08x via %-5s\t%s", target, inst, found_section);
#else
    printt(JTRACE, "Jump to %08x via %s", target, inst);
#endif
  } else {
    printt(JTRACE, "Jump to %08x via %s", target, inst);
  }
#endif
}

def_EHelper(beq) {
    rtl_jrelop(s, RELOP_EQ, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(bne) {
    rtl_jrelop(s, RELOP_NE, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(blt) {
    rtl_jrelop(s, RELOP_LT, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(bge) {
    rtl_jrelop(s, RELOP_GE, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(bltu) {
    rtl_jrelop(s, RELOP_LTU, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(bgeu) {
    rtl_jrelop(s, RELOP_GEU, dsrc1, dsrc2, id_dest->imm);
}

def_EHelper(b) {
  trace_jump(id_src1->imm, "b");
    rtl_j(s, id_src1->imm);
}

def_EHelper(bl) {
    rtl_li(s, ddest, id_src2->imm);
  trace_jump(id_src1->imm, "bl");
    rtl_j(s, id_src1->imm);
}

def_EHelper(jirl) {
    rtl_addi(s, s0, dsrc1, id_src2->imm);
    rtl_li(s, ddest, s->snpc);
  trace_jump(*s0, "jirl");
    rtl_jr(s, s0);
}