#include "../local-include/intr.h"
#include "../local-include/csr.h"

def_EHelper(nemu_trap) {
  // save_globals(s);
  rtl_hostcall(s, HOSTCALL_EXIT, NULL, &cpu.gpr[4]._32, NULL, 0); // gpr[4] is $v0
  // longjmp_exec(NEMU_EXEC_END);
}

def_EHelper(print_led) {
  Log("print_led called at PC: %08x, current $t0: %08x\n", cpu.pc, cpu.gpr[12]._32);
}

def_EHelper(inv) {
  Err("Invalid instruction at PC: %08x (%08x)", s->pc, s->isa.instr.val);
#ifdef CONFIG_EXT_HANDLE_EX_INE
  rtl_li(s, s1, s->pc);
  rtl_hostcall(s, HOSTCALL_TRAP, s0, s1, NULL, EX_INE);
  rtl_jr(s, s0);
#else
  rtl_hostcall(s, HOSTCALL_INV, NULL, NULL, NULL, 0);
#endif
}

// below are some inst that did not implement

def_EHelper(preld) {
}

def_EHelper(ibar) {
}

def_EHelper(dbar) {
}

def_EHelper(cacop) {
  if(CRMD->plv == 0x3 && ((id_src1->imm) & (0x10))){
    exception(EX_IPE);
  }

  rtl_addi(s, s0, ddest, id_src2->imm);
//      printf("!!!!!! CACOP %08x\n", *s0);
  isa_mmu_translate(*s0, 4, MEM_TYPE_READ);
}
