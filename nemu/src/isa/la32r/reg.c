#include <isa.h>
#include "local-include/reg.h"
#include "local-include/csr.h"

const char *regsl[] = {
    "r0", "ra", "tp", "sp", "a0", "a1", "a2", "a3",
    "a4", "a5", "a6", "a7", "t0", "t1", "t2", "t3",
    "t4", "t5", "t6", "t7", "t8", "x", "fp", "s0",
    "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8"
};

void isa_reg_display() {
  printf(ANSI_FMT("CPU status:\n", ANSI_FG_YELLOW));
  int i;
  for (i = 0; i < 32; i ++) {
    printf("%3s (r%-2d): %08x (%10u)", regsl[i], i, cpu.gpr[i]._32, cpu.gpr[i]._32);
    if (i % 4 == 3) printf("\n");
  }
  printf("last PC: %08x, PC: %08x\n", cpu.idle_pc, cpu.pc);
  if(isa_mmu_state() == MMU_DIRECT){
    printf("Current MMU state is: MMU_DIRECT\n");
  }else{
    printf("Current MMU state is: MMU_TRANSLATE\n");
  }
  printf("CRMD: %08x,    PRMD: %08x,   EUEN: %08x\n", csr_array[0], csr_array[1], csr_array[2]);
  printf("ECFG: %08x,   ESTAT: %08x,    ERA: %08x\n", csr_array[4], csr_array[5], csr_array[6]);
  printf("BADV: %08x,  EENTRY: %08x, LLBCTL: %08x\n", csr_array[7], csr_array[12], csr_array[96]);
  printf("SAVE0: %08x,  SAVE1: %08x,  SAVE2: %08x,  SAVE3: %08x\n", csr_array[0x30], csr_array[0x31], csr_array[0x32], csr_array[0x33]);
  printf("cpu.ll_bit: %d\n",cpu.ll_bit);
  printf("INDEX: %08x, TLBEHI: %08x, TLBELO0: %08x, TLBELO1: %08x\n", csr_array[10], csr_array[11], csr_array[12], csr_array[13]);
  printf("ASID: %08x, TLBRENTRY: %08x, DMW0: %08x, DMW1: %08x\n", csr_array[18], csr_array[88], csr_array[180], csr_array[181]);
}

void isa_reg_display2() {
  printf(ANSI_FMT("CPU status:\n", ANSI_FG_YELLOW));
  int i;
  for (i = 0; i < 32; i ++) {
    printf("%3s (r%-2d): %08x (%10u)", regsl[i], i, cpu.gpr[i]._32, cpu.gpr[i]._32);
    if (i % 4 == 3) printf("\n");
  }
  printf("last PC: %08x, PC: %08x\n", cpu.idle_pc, cpu.pc);
  if(isa_mmu_state() == MMU_DIRECT){
    printf("Current MMU state is: MMU_DIRECT\n");
  }else{
    printf("Current MMU state is: MMU_TRANSLATE\n");
  }
  printf("CRMD: %08x,    PRMD: %08x,   EUEN: %08x\n", cpu.crmd, cpu.prmd, cpu.euen);
  printf("ECFG: %08x,   ESTAT: %08x,    ERA: %08x\n", cpu.ecfg, cpu.estat, cpu.era);
  printf("BADV: %08x,  EENTRY: %08x, LLBCTL: %08x\n", cpu.badv, cpu.eentry, cpu.llbctl);
  printf("SAVE0: %08x,  SAVE1: %08x,  SAVE2: %08x,  SAVE3: %08x\n", cpu.save0, cpu.save1, cpu.save2, cpu.save3);
  printf("cpu.ll_bit: %d\n", cpu.ll_bit);
  printf("INDEX: %08x, TLBEHI: %08x, TLBELO0: %08x, TLBELO1: %08x\n", cpu.tlbidx, cpu.tlbehi, cpu.tlbelo0, cpu.tlbelo1);
  printf("ASID: %08x, TLBRENTRY: %08x, DMW0: %08x, DMW1: %08x\n", cpu.asid, cpu.tlbrentry, cpu.dmw0, cpu.dmw1);
}

word_t isa_reg_str2val(const char *s, bool *success) {
  int i;
  *success = true;
  if (strcmp("pc", s) == 0) return cpu.pc;
  for (i = 0; i < 32; i ++) {
    if (strcmp(regsl[i], s) == 0) return reg_l(i);
  }
  *success = false;
  return 0;
}
