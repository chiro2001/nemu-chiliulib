#include <utils.h>
#include <cpu/ifetch.h>
#include <rtl/rtl.h>

uint32_t pio_read(ioaddr_t addr, int len);
void pio_write(ioaddr_t addr, int len, uint32_t data);

void set_nemu_state(int state, vaddr_t pc, int halt_ret) {
  nemu_state.state = state;
  nemu_state.halt_pc = pc;
  nemu_state.halt_ret = halt_ret;
}

static inline void invalid_instr(vaddr_t thispc) {
  set_nemu_state(NEMU_ABORT, thispc, -1);
}

def_rtl(fpcall, rtlreg_t *dest, const rtlreg_t *src1, const rtlreg_t *src2, uint32_t cmd) { }

def_rtl(hostcall, uint32_t id, rtlreg_t *dest, const rtlreg_t *src1,
        const rtlreg_t *src2, word_t imm) {
  switch (id) {
    case HOSTCALL_EXIT:
      set_nemu_state(NEMU_END, s->pc, *src1);
      break;
    case HOSTCALL_INV: invalid_instr(s->pc); break;
    case HOSTCALL_FP:  rtl_fpcall(s, dest, src1, src2, imm); break;
#ifdef CONFIG_DEVICE
    case HOSTCALL_PIO: {
      int width = imm & 0xf;
      bool is_in = ((imm & ~0xf) != 0);
      if (is_in) *dest = pio_read(*src1, width);
      else pio_write(*dest, width, *src1);
      break;
    }
#endif
    default: isa_hostcall(id, dest, src1, src2, imm); break;
  }
}
