#include <cpu/cpu.h>
#include <cpu/exec.h>
#include <isa-all-instr.h>
#include <locale.h>

/* The assembly code of instructions executed is only output to the screen
 * when the number of instructions executed is less than this value.
 * This is useful when you use the `si' command.
 * You can modify this value as you want.
 */
#define MAX_INSTR_TO_PRINT 10

CPU_state cpu = {};
jmp_buf exception_jbuf;
uint64_t g_nr_guest_instr = 0;
static uint64_t g_timer = 0; // unit: us
const rtlreg_t rzero = 0;
rtlreg_t tmp_reg[4];

void device_update();
void fetch_decode(Decode *s, vaddr_t pc);
void timer_action();


static void trace(Decode *_this) {
#ifdef CONFIG_ITRACE
  printt(ITRACE, "%s", _this->logbuf);
#endif
}

// Instruction ring buffer

#ifdef CONFIG_ITRACE
#define IRINGBUF_SIZE 10

static struct ringbuf {
  int index;
  int total_cnt;
  Decode inst[IRINGBUF_SIZE];
} iringbuf;

static void iringbuf_init() {
  iringbuf.index = 0;
  iringbuf.total_cnt = 0;
}

static void iringbuf_log(Decode *s) {
  iringbuf.total_cnt++;
  memcpy(&(iringbuf.inst[iringbuf.index]), s, sizeof(Decode));
  iringbuf.index++;
  if (iringbuf.index == IRINGBUF_SIZE) {
    iringbuf.index = 0;
  }
}

void iringbuf_print() {
  printf(ANSI_FMT("Last %d instructions executed:\n", ANSI_FG_YELLOW), (iringbuf.total_cnt <= IRINGBUF_SIZE) ? iringbuf.total_cnt : IRINGBUF_SIZE);
  if (iringbuf.total_cnt <= IRINGBUF_SIZE) {
    for (int i = 0; i < iringbuf.total_cnt; i++) {
      printf("%08x %s\n", iringbuf.inst[i].pc, iringbuf.inst[i].logbuf);
    }
  }
  else {
    for (int i = iringbuf.index; i < IRINGBUF_SIZE; i++) {
      printf("%08x %s\n", iringbuf.inst[i].pc, iringbuf.inst[i].logbuf);
    }
    for (int i = 0; i < iringbuf.index; i++) {
      printf("%08x %s\n", iringbuf.inst[i].pc, iringbuf.inst[i].logbuf);
    }
  }
}

#endif

#include <isa-exec.h>

#define FILL_EXEC_TABLE(name) [concat(EXEC_ID_, name)] = concat(exec_, name),
static const void* g_exec_table[TOTAL_INSTR] = {
    MAP(INSTR_LIST, FILL_EXEC_TABLE)
};

static void fetch_decode_exec_updatepc(Decode *s) {
  // IF, ID
  fetch_decode(s, cpu.pc);
  trace(s);
  // EX, MEM, WB
  // dnpc might be updated due to reasons
  s->EHelper(s);
  // PC <- dnpc
  cpu.pc = s->dnpc;
}

void statistic() {
#define NUMBERIC_FMT MUXDEF(CONFIG_TARGET_AM, "%ld", "%ld")
  Log("host time spent = " NUMBERIC_FMT " us", g_timer);
  Log("total guest instructions = " NUMBERIC_FMT, g_nr_guest_instr);
  if (g_timer > 0) Log("simulation frequency = " NUMBERIC_FMT " instr/s (%.3f MHz)", g_nr_guest_instr * 1000000 / g_timer, (float) g_nr_guest_instr / g_timer);
  else Log("Finish running in less than 1 us and can not calculate the simulation frequency");
}

void assert_fail_msg() {
  IFDEF(CONFIG_ITRACE, iringbuf_print());
  isa_reg_display();
  statistic();
}

void fetch_decode(Decode *s, vaddr_t pc) {
  s->pc = pc;
  s->snpc = pc;
  IFDEF(CONFIG_ITRACE, log_bytebuf[0] = '\0');
  int idx = isa_fetch_decode(s);
  s->dnpc = s->snpc;
  s->EHelper = g_exec_table[idx];

  // Disassembly
  IFDEF(CONFIG_ITRACE, snprintf(s->logbuf, sizeof(s->logbuf), " %16llu" ":   %s%*.s%s",
      g_nr_guest_instr, log_bytebuf, 30 - (12 + 3 * (int)(s->snpc - s->pc)), "", log_asmbuf));
  IFDEF(CONFIG_ITRACE, iringbuf_log(s));
}

word_t paddr_read(paddr_t addr, int len);

/**
 * Execute
 * @param n
 */
void cpu_exec(uint64_t n) {
  switch (nemu_state.state) {
    case NEMU_END: case NEMU_ABORT:
      printf("Program execution has ended. To restart the program, exit NEMU and run again.\n");
      return;
    default: nemu_state.state = NEMU_RUNNING;
  }

  uint64_t timer_start = get_time();

  IFDEF(ITRACE, iringbuf_init());

  Decode s;
  int exception_no;
  // When n is -1 then this will forever run.
  for (;n > 0; n --) {
    // interruptions
    word_t intr = isa_query_intr();
    if (intr != INTR_EMPTY) {
      cpu.pc = raise_intr(intr, cpu.pc);
      n++; // for difftest sync
      continue;
    }
    // execute
    if (!(exception_no = setjmp(exception_jbuf))) {
      fetch_decode_exec_updatepc(&s);
      g_nr_guest_instr ++;
      if (nemu_state.state != NEMU_RUNNING) break;
      IFDEF(CONFIG_DEVICE, device_update());
    }
    else {
      // Exception occurs!
      cpu.pc = raise_intr(exception_no, s.pc);
    }
    // timer
    IFNDEF(CONFIG_COMPILE_NEMU_REF, timer_action());
    IFNDEF(CONFIG_COMPILE_NEMU_REF, isa_emit_int());
  }

  uint64_t timer_end = get_time();
  g_timer += timer_end - timer_start;

  switch (nemu_state.state) {
    case NEMU_RUNNING: nemu_state.state = NEMU_STOP; break;

    case NEMU_END: case NEMU_ABORT:
      Log("%s at pc = " FMT_WORD,
          (nemu_state.state == NEMU_ABORT ? ANSI_FMT("ABORT", ANSI_FG_RED) :
           (nemu_state.halt_ret == 0 ? ANSI_FMT("HIT GOOD TRAP", ANSI_FG_GREEN) :
            ANSI_FMT("HIT BAD TRAP", ANSI_FG_RED))),
          nemu_state.halt_pc);
      if (nemu_state.state == NEMU_ABORT || nemu_state.halt_ret != 0) {
        IFDEF(CONFIG_ITRACE, iringbuf_print());
        isa_reg_display();
      }
      // fall through
    case NEMU_QUIT: statistic();
  }
}

void exception(int ex_cause) {
  longjmp(exception_jbuf, ex_cause);
}