/**
 * @file nemu-ref.c
 * @brief NEMU as REF
 * @author Weitong Wang, Hans
 */
#include <isa.h>
#include <memory/paddr.h>
#include <cpu/cpu.h>
#include <difftest-def.h>

static_assert(MUXDEF(CONFIG_COMPILE_NEMU_REF, 1, 0), "Must compile with definition: CONFIG_COMPILE_NEMU_REF");

#ifdef CONFIG_LARGE_COPY
static void nemu_large_memcpy(void *dest, void *src, size_t n) {
  uint64_t *_dest = (uint64_t *)dest;
  uint64_t *_src  = (uint64_t *)src;
  while (n >= sizeof(uint64_t)) {
    if (*_src != 0) {
      *_dest = *_src;
    }
    _dest++;
    _src++;
    n -= sizeof(uint64_t);
  }
  if (n > 0) {
    uint8_t *dest8 = (uint8_t *)_dest;
    uint8_t *src8  = (uint8_t *)_src;
    while (n > 0) {
      *dest8 = *src8;
      dest8++;
      src8++;
      n--;
    }
  }
}
#endif

/** FOR DIFFTEST **/

/**
 * 在 NEMU 和 DUT 之间拷贝内存
 * @param nemu_addr NEMU 中的目标 / 源地址
 * @param dut_buf DUT 中的内存起点指针
 * @param n 拷贝的字节数
 * @param direction 拷贝方向
 */
void difftest_memcpy(paddr_t nemu_addr, void *dut_buf, size_t n, bool direction) {
#ifdef CONFIG_LARGE_COPY
  if (direction == DIFFTEST_TO_REF) nemu_large_memcpy(guest_to_host(nemu_addr), dut_buf, n);
  else nemu_large_memcpy(dut_buf, guest_to_host(nemu_addr), n);
#else
  if (direction == DIFFTEST_TO_REF) memcpy(guest_to_host(nemu_addr), dut_buf, n);
  else memcpy(dut_buf, guest_to_host(nemu_addr), n);
#endif
  Log("Copyied %lu byte(s) %s at address 0x%08x", n, direction == DIFFTEST_TO_DUT ? "from REF to DUT" : "from DUT to REF", nemu_addr);
}

/**
 * 在 NEMU 和 DUT 之间的拷贝寄存器（REG 和 CSR）及 PC
 * 见 src/isa/la32r/include/isa-def.h 的 la32r_CPU_State 中的注释
 * @param dut
 * @param direction
 * @param do_csr
 */
void difftest_regcpy(void *dut, bool direction, bool do_csr) {
  isa_difftest_regcpy(dut, direction, do_csr);
}

word_t difftest_get_pc() {
  return cpu.pc;
}

/**
 * DUT 向 NEMU 通报计时器状态
 * @param dut
 */
void difftest_timercpy(void *dut) {
  isa_difftest_timercpy(dut);
}

/**
 * DUT 向 NEMU 通报 ESTAT 寄存器
 * @param index
 * @param mask
 */
void difftest_estat_sync(uint32_t index, uint32_t mask) {
  isa_difftest_estat_sync(index, mask);
}

/**
 * DUT 向 NEMU 同步指定项的 TLB
 * @param index
 * @param dut
 */
void difftest_tlbcpy(uint32_t index, void *dut) {
  isa_difftest_tlbcpy(index, dut);
}

/**
 * 当 NEMU 作为参考平台时，运行 TLBFILL 前要用这个函数来同步写入的项号
 * @param index
 */
void difftest_tlbfill_index_set(uint32_t index) {
  isa_difftest_tlbfill_index_set(index);
}

/**
 * 同步 CSR
 * @param dut
 * @param direction
 */
void difftest_csrcpy(void *dut, bool direction) {
  isa_difftest_csrcpy(dut, direction);
}

void difftest_uarchstatus_cpy(void *dut, bool direction) {
  isa_difftest_uarchstatus_cpy(dut, direction);
}

int difftest_store_commit(uint64_t saddr, uint64_t sdata) {
#ifdef CONFIG_DIFFTEST_STORE_COMMIT
  return check_store_commit(saddr, sdata);
#else
  return 0;
#endif
}
//#endif

/**
 * NEMU 单步执行指定数量的指令
 * @param n
 */
void difftest_exec(uint64_t n) {
  cpu_exec(n);
}

//#ifdef CONFIG_GUIDED_EXEC
void difftest_guided_exec(void * guide) {static int difftest_port = 1234;
  isa_difftest_guided_exec(guide);
}
//#endif

/**
 * NEMU 发起中断
 * 只有硬中断和计时器要用
 * @param NO
 */
void difftest_raise_intr(word_t NO) {
  isa_difftest_raise_intr(NO);
}

int difftest_check_end() {
  return 0;
}

IFDEF(CONFIG_DEVICE, void init_device());
IFDEF(CONFIG_EXT_REF_LOG, void init_log(const char *log_file));

/**
 * 初始化 NEMU
 */
void difftest_init() {
  IFDEF(CONFIG_EXT_REF_LOG, init_log(CONFIG_EXT_REF_LOG_PATH));
  init_mem();
  init_isa();
  IFDEF(CONFIG_DEVICE, init_device());
  /* Init sections info */
  IFDEF(CONFIG_FTRACE, elf_init());
}

/**
 * 释放资源
 */
void difftest_release() {
  release_mem();
}

/** END OF DIFFTEST **/

/** FOR BACKEND TEST **/

/**
 * 加载镜像到内存指定地址
 * @param file_name
 * @param addr
 * @return 成功为 0，出错为其他（-1 为文件不能打开，1 为载入失败）
 */
int backend_load_image(const char *file_name, paddr_t addr) {
  return load_img(file_name, addr);
}

word_t backend_read_memory(paddr_t addr) {

}

/**
 * 初始化 NEMU
 */
void backend_init() {
  init_mem();
  init_isa();
}

/** END OF BACKEND TEST **/

