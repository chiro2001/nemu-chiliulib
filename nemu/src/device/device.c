#include <common.h>
#include <utils.h>
#include <SDL2/SDL.h>

void init_map();
void init_uart();
void init_vga();
void vga_update_screen();

#define TIMER_HZ 60

void device_update() {
  static uint64_t last = 0;
  static uint8_t div = 0;
  const uint8_t divider = CONFIG_EXT_DEVICE_DIVIDER;
  if (div != divider) {
    div++;
    return;
  } else {
    div = 0;
  }
  uint64_t now = get_time();
  if (now - last < 1000000 / TIMER_HZ) {
    return;
  }
  last = now;

  IFDEF(CONFIG_VGA_EN, vga_update_screen());

  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT:
        nemu_state.state = NEMU_QUIT;
        break;
// #ifdef CONFIG_HAS_KEYBOARD
#if defined(CONFIG_HAS_KEYBOARD) && !defined(CONFIG_COMPILE_NEMU_REF)
      // If a key was pressed
      case SDL_KEYDOWN:
      case SDL_KEYUP: {
        uint8_t k = event.key.keysym.scancode;
        bool is_keydown = (event.key.type == SDL_KEYDOWN);
        send_key(k, is_keydown);
        break;
      }
#endif
      default:
        break;
    }
  }
}

void sdl_clear_event_queue() {
  SDL_Event event;
  while (SDL_PollEvent(&event));
}

void init_device() {
  init_map();

  IFDEF(CONFIG_UART_EN, init_uart());
  IFDEF(CONFIG_VGA_EN, init_vga());
}
