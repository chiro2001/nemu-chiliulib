#include <device/map.h>
#include <stdio.h>
#include <stdlib.h>
#include <utils.h>

/** NS16550A **/

#define RBR_THR_OFFSET 0
#define IER_OFFSET 1
#define IIR_FCR_OFFSET 2
#define LCR_OFFSET 3
#define MCR_OFFSET 4
#define LSR_OFFSET 5
#define MSR_OFFSET 6
#define SCR_OFFSET 7

union {
  struct {
    uint8_t pad : 4;
    bool MSR_change : 1;
    bool LSR_change : 1;
    bool THR_empty : 1;
    bool data_available : 1;
  };
  uint8_t val;
} IER;

bool enable_fifo;
bool enable_uart_print = true;

static uint8_t *uart_base = NULL;

#define UART_BUF_SIZE 1024

char g_uart_buf[UART_BUF_SIZE];
char *g_uart_p;

// #define UART_REAL_TIME_DISPLAY 1

#define printu(module, format, ...)                                                                                                                \
  IFDEF(                                                                                                                                           \
      concat3(CONFIG_, module, _EN),                                                                                                               \
      do {                                                                                                                                         \
        time_t raw_time = get_time_sec();                                                                                                          \
        struct tm *ptminfo = localtime(&raw_time);                                                                                                 \
        _Err(CONFIG_PRINT_DEVICE, "\r" REFCHECK ANSI_FMT("[%12lu][%02d-%02d-%02d %02d:%02d:%02d][%6s] ", ANSI_FG_GREEN) format "\n",               \
             g_nr_guest_instr, ptminfo->tm_year + 1900, ptminfo->tm_mon + 1, ptminfo->tm_mday, ptminfo->tm_hour, ptminfo->tm_min, ptminfo->tm_sec, \
             str(module), ##__VA_ARGS__);                                                                                                          \
      } while (0))
// Log to stdout when using WT
#define printl(module, format, ...)                                                                                                                \
  IFDEF(                                                                                                                                           \
      concat3(CONFIG_, module, _EN),                                                                                                               \
      do {                                                                                                                                         \
        time_t raw_time = get_time_sec();                                                                                                          \
        struct tm *ptminfo = localtime(&raw_time);                                                                                                 \
        _Log(CONFIG_PRINT_DEVICE, REFCHECK ANSI_FMT("[%12lu][%02d-%02d-%02d %02d:%02d:%02d][%6s] ", ANSI_FG_GREEN) format "\n",                    \
             g_nr_guest_instr, ptminfo->tm_year + 1900, ptminfo->tm_mon + 1, ptminfo->tm_mday, ptminfo->tm_hour, ptminfo->tm_min, ptminfo->tm_sec, \
             str(module), ##__VA_ARGS__);                                                                                                          \
      } while (0))

void uart_putc_buffed(char ch) {
  if (g_uart_p - g_uart_buf < UART_BUF_SIZE)
    *(g_uart_p++) = (char) (ch == '\n' ? '\0' : ch);
  if ((ch == '\0' || ch == '\n')) {
    if (enable_uart_print) {
      if (*g_uart_buf) {
        MUXDEF(UART_REAL_TIME_DISPLAY, printu(UART, "%s", g_uart_buf), MUXDEF(CONFIG_X_WT, printl(UART, "%s", g_uart_buf), printd(UART, "%s", g_uart_buf)));
      } else {
        if ((int) ch != 0xFF) {
          printd(UART, "%s", "[EMPTY STR]");
        }
      }
    }
    g_uart_p = g_uart_buf;
    memset(g_uart_buf, 0, sizeof(g_uart_buf));
  }
}

static void uart_putc(char ch) {
  uart_putc_buffed(ch);
#ifdef UART_REAL_TIME_DISPLAY
  if (ch && ch != '\n') {
    putchar(ch);
    fflush(stdout);
  }
#endif
}

int uart_last_input = 0;
int uart_last_input_value = 0;

static void uart_io_handler(uint32_t offset, int len, bool is_write) {
  Assert(len == 1, "len != 1! (len = %d)", len);
  switch (offset) {
    /* We bind the serial port with the host stderr in NEMU. */
    case RBR_THR_OFFSET:
      if (is_write)
        uart_putc((char) uart_base[offset]);
      else {
        uart_base[offset] = uart_last_input;
        Log("uart_last_input_value = %d", uart_last_input_value);
        uart_last_input_value = 0;
      }
      break;
    case IER_OFFSET:
      if (is_write)
        IER.val = (uint8_t) uart_base[offset];
      else
        uart_base[offset] = IER.val;
      break;
    case IIR_FCR_OFFSET:
      if (is_write)
        enable_fifo = ((uint8_t) uart_base[offset]) & 0x1;
      else
        uart_base[offset] = enable_fifo ? 0xC1 : 0x01;
      break;
    case LCR_OFFSET:
      break;
    case MCR_OFFSET:
      // Log("MCR_OFFSET(4)! %s", (is_write ? "write" : "read"));
      break;
    case LSR_OFFSET:
      // Log("LSR_OFFSET(5)! %s", (is_write ? "write" : "read"));
      uart_base[offset] = 0x60;
      break;
    case MSR_OFFSET:
      break;
    case SCR_OFFSET:
      break;
    default:
      panic("do not support offset = %d", offset);
  }
  // Log("uart_io_handler(offset=%d, len=%d, %s) data=0x%x", offset, len, (is_write ? "write" : "read"), uart_base[offset]);
}

const uint64_t uart_div_max = 0xffffff;
uint64_t uart_div = uart_div_max;

int uart_capture_input() {
  if (uart_div || uart_last_input_value) {
    if (uart_div) uart_div--;
    return uart_last_input_value;
  } else {
    uart_div = uart_div_max;
  }
  fd_set rfds;
  struct timeval tv;
  int ch = 0;
  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  tv.tv_sec = 0;
  tv.tv_usec = 10;
  if (select(1, &rfds, NULL, NULL, &tv) > 0) ch = getchar();
  if (ch) {
    uart_last_input = ch;
    uart_last_input_value = ch;
  }
  return uart_last_input_value;
}

void init_uart() {
  uart_base = new_space(8);
  add_mmio_map("uart", CONFIG_UART_MMIO, uart_base, 8, uart_io_handler);
  g_uart_p = g_uart_buf;
}
