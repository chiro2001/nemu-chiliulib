#include <common.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef CONFIG_FTRACE

typedef struct {
  uint32_t addr;
  char name[32];
} SectionNode;

// 升序排列的段列表
SectionNode *sections = NULL;
SectionNode *sections_tail = NULL;

SectionNode sections_ignores[] = {
    {0, "serial_put_char"},
    {0, "data_out"},
    {0, "sys_uart_putc"},
    {0, "mach_logger"},
    {0, "strlen"},
    {0, "vsnprintf"},
    {0, "__udivdi3"},
    {0, "__umoddi3"},
    {0, "format_int"},
    {0, "memcpy"},
    {0, "__stdio_write"},
    {0, "control_construct"},
    {0, "do_init_vfs"},
    {0, "mutex_init"},
    {0, "init_list_head"},
    {0, ""}};

/**
 * @brief 读取elf文件信息，储存到有序列表中
 * @param filepath elf文件路径
 */
void elf_init_info(const char *filepath) {
  static char buf[256];
  char line[1024];
  int line_count = 0;
  char buf_line_number[32], buf_addr[12], buf_unknown[12], buf_type[32],
      buf_area[24], buf_type2[32], buf_1[32], buf_name[32];
  // sprintf(buf, CONFIG_EXT_READELF " \"%s\" -s | grep FUNC | wc -l",
  //         filepath);
  sprintf(buf, CONFIG_EXT_READELF " \"%s\" -s | grep \"FUNC\\|NOTYPE\" | wc -l",
          filepath);
  Log("execute: %s", buf);
  FILE *f = popen(buf, "r");
  int r = fscanf(f, "%d", &line_count);
  assert(r);
  // Log("ELF Read %d Sections.", (int)line_count);
  sections = malloc(sizeof(SectionNode) * line_count);
  memset(sections, 0, sizeof(SectionNode) * line_count);
  assert(sections);
  sections_tail = sections;
  // sprintf(buf, CONFIG_EXT_READELF " \"%s\" -s | grep FUNC | sort -k 2",
  //         filepath);
  sprintf(buf,
          CONFIG_EXT_READELF
          " \"%s\" -s | grep \"FUNC\\|NOTYPE\" | sort "
          "-k 2",
          filepath);
  // system(buf);
  f = popen(buf, "r");
  bool read_elf_head = false;
  while (fgets(line, 1024, f)) {
    //  2103: 80038bb4    12 FUNC    GLOBAL DEFAULT    1 __am_gpu_status
    // Log("Read line: %s", line);
    int ret =
        sscanf(line, "%s%s%s%s%s%s%s%s", buf_line_number, buf_addr, buf_unknown,
               buf_type, buf_area, buf_type2, buf_1, buf_name);
    // Assert((ret == 8 && !read_elf_head), "sscanf for elf info should be 8,
    // ret = %d", ret);
    if (!read_elf_head) {
      read_elf_head = true;
      continue;
    }
    // if (ret != 8) {
    //   Log("sscanf for elf info should be 8, ret = %d", ret);
    // }
    if (ret != 8) continue;
    strcpy(sections_tail->name, buf_name);
    sscanf(buf_addr, "%x", &sections_tail->addr);
    // #define CONFIG_READELF_OFFSET 0xc0000040
    //     if (sections_tail->addr >= CONFIG_READELF_OFFSET)
    //       sections_tail->addr -= CONFIG_READELF_OFFSET;
    //     else
    //       continue;
    //     sections_tail->addr += CONFIG_MBASE;
    // Log("ELF Read: " FMT_WORD " : %s", sections_tail->addr,
    // sections_tail->name);
    sections_tail++;
  }
  sections_tail--;
  SectionNode *p = sections;
  // size_t p_len = 10;
  size_t p_len = CONFIG_EXT_SHOW_ELF_SYMBOL_NUMBER;
  while (p != sections_tail && (p_len--)) {
    if (!p->name || (p->name && !*p->name)) continue;
    // if (!p->addr) continue;
    Log(FMT_WORD " %s", p->addr, p->name);
    p++;
  }
  pclose(f);
}

/**
 * @brief 释放 elf 解析需要的内存
 */
void elf_release() {
  if (sections) free(sections);
  sections = NULL;
  sections_tail = NULL;
}

/**
 * @brief 二分查找找到对应的段名称
 * @param pc 目标地址
 * @param left 左端点
 * @param right 右端点
 * @return size_t 段index
 */
size_t find_section_inner(uint32_t pc, size_t left, size_t right) {
  if (left == right || left + 1 == right) return left;
  size_t mid = (left + right) / 2;
  if (sections[mid].addr <= pc && sections[mid + 1].addr > pc) {
    return mid;
  }
  if (sections[mid].addr < pc) {
    return find_section_inner(pc, mid, right);
  } else {
    return find_section_inner(pc, left, mid);
  }
}

/**
 * @brief 查找段名称
 * @param pc 目标地址
 * @return const char* 段名称
 */
const char *find_section(uint32_t pc) {
  for (SectionNode *s = sections_ignores; s->name && *s->name; s++)
    if (pc == s->addr) return NULL;
  static char buf[64];
  if (!sections) return "--";
  if (pc < sections[0].addr) return "HEAD";
  if (pc >= sections_tail->addr) return "TAIL";
  SectionNode *t =
      &sections[find_section_inner(pc, 0, (sections_tail - sections - 1))];
  for (SectionNode *s = sections_ignores; s->name && *s->name; s++)
    if (t->addr == s->addr) return NULL;
  if (pc != t->addr)
    sprintf(buf, "%s+0x%x", t->name, (pc - t->addr));
  else
    sprintf(buf, "<%s>", t->name);
  // return sections[find_section_inner(pc, 0, (sections_tail - sections - 1))]
  //     .name;
  return buf;
}

/**
 * @brief 初始化
 */
void elf_init() {
  char *path = CONFIG_EXT_ELF_PATH;
  assert(path);
  if (*path != '/') {
    char *chiplab_home = getenv("CHIPLAB_HOME");
    Assert(chiplab_home != NULL, "Plz set $CHIPLAB_HOME in your env! Or use abs path.");
    path = (char *) malloc(2048);
    sprintf(path, "%s/%s", chiplab_home, CONFIG_EXT_ELF_PATH);
  }
  Log("ELF path: %s", path);
  elf_init_info(path);

  // init ignores
  for (SectionNode *s = sections_ignores; s->name && *s->name; s++) {
    for (SectionNode *i = sections; i != sections_tail; i++) {
      if (i->name && strcmp(i->name, s->name) == 0) {
        s->addr = i->addr;
        // Log("Ignore section %s at 0x%08x", s->name, s->addr);
        break;
      }
    }
  }
}

#else

const char *find_section(uint32_t pc) { return "--"; }

#endif