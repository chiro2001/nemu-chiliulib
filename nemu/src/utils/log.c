#include <malloc.h>
#include <utils.h>

#ifndef CONFIG_TRACE_START
#define CONFIG_TRACE_START 0
#define CONFIG_TRACE_END 0
#endif

FILE *log_fp = NULL;
char *log_path = NULL;
char *log_tail_path = NULL;
uint64_t log_cnt = 0;

void init_log(const char *log_file) {
  if (log_file == NULL) return;
  if (!log_tail_path) log_tail_path = (char *) malloc(2048);
  assert(log_tail_path);
  if (!log_path) log_path = (char *) malloc(2048);
  assert(log_path);
  if (log_file != log_path) strcpy(log_path, log_file);
  // default: %s.tail.log
  sprintf(log_tail_path, CONFIG_TAIL_LOG_LAST_EXT, log_file);
  log_fp = fopen(log_file, "w");
  if (log_fp) {
    // first init
    if (log_file != log_path) Log("Initialised log file %s", log_file);
    else Log("Refreshed log tail after %d logs", CONFIG_TAIL_LOG_SIZE);
    Log("Set tail log file path %s", log_tail_path);
  } else {
    Err("Error occurred while opening log file %s", log_file);
  }
}


char log_bytebuf[50] = {};
char log_asmbuf[168] = {};
