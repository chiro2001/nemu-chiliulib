#include <common.h>
#include MUXDEF(CONFIG_TIMER_GETTIMEOFDAY, <sys / time.h>, <time.h>)

#ifndef WIN32
IFDEF(CONFIG_TIMER_CLOCK_GETTIME,
      static_assert(CLOCKS_PER_SEC == 1000000, "CLOCKS_PER_SEC != 1000000"));
IFDEF(CONFIG_TIMER_CLOCK_GETTIME,
      static_assert(sizeof(clock_t) == 8, "sizeof(clock_t) != 8"));
#endif

static uint64_t boot_time = 0;
// static clock_t start_t = 0;

uint64_t get_time_internal() {
#if defined(CONFIG_TIMER_GETTIMEOFDAY)
  struct timeval now;
  gettimeofday(&now, NULL);
  uint64_t us = now.tv_sec * 1000000 + now.tv_usec;
#else
  struct timespec now;
#ifdef CLOCK_MONOTONIC_COARSE
  clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
#else
  clock_gettime(CLOCK_MONOTONIC, &now);
#endif
  uint64_t us = now.tv_sec * 1000000 + now.tv_nsec / 1000;
#endif
  return us;
}

time_t get_time_sec() {
#if defined(CONFIG_TIMER_GETTIMEOFDAY)
  struct timeval now;
  gettimeofday(&now, NULL);
#else
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
#endif
  return now.tv_sec;
}

uint64_t get_time() {
  if (boot_time == 0) boot_time = get_time_internal();
  // if (start_t == 0) start_t = clock();
  uint64_t now = get_time_internal();
  return now - boot_time;
}
