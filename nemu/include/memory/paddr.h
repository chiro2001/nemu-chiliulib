#ifndef __MEMORY_DDR_H__
#define __MEMORY_DDR_H__

#include <common.h>
#include <host.h>

#ifdef __cplusplus
extern "C" {
#endif

#define RESET_VECTOR CONFIG_PC_RESET_OFFSET

void init_mem();
void release_mem();

extern uint8_t *pmem;

/* convert the guest physical address in the guest program to host virtual address in NEMU */
static inline uint8_t *guest_to_host(paddr_t paddr) {
  return pmem + paddr - CONFIG_MBASE;
}

/* convert the host virtual address in NEMU to guest physical address in the guest program */
static inline paddr_t host_to_guest(uint8_t *haddr) {
  return haddr + CONFIG_MBASE - pmem;
}

static inline bool in_pmem(paddr_t addr) {
  // Log("in_pmem(0x%08x) %s", addr, (addr >> 24 != 0x1f ? "yes" : "no"));
  return addr >> 24 != 0x1f;
  // TODO: change it
  paddr_t mbase_mask = CONFIG_MBASE - 1;
  paddr_t msize_mask = CONFIG_MSIZE - 1;
  bool mbase_align = (CONFIG_MBASE & mbase_mask) == 0;
  bool msize_align = (CONFIG_MSIZE & msize_mask) == 0;
  bool msize_inside_mbase = msize_mask <= mbase_mask;
  if (addr == 0x1fe001e5) {
    Log("trap!");
  }
  if (mbase_align && msize_align && msize_inside_mbase) {
    return (addr & ~msize_mask) == CONFIG_MBASE;
  } else {
    return (addr >= CONFIG_MBASE) && (addr <= (paddr_t) CONFIG_MBASE + (CONFIG_MSIZE - 1));
  }
}

word_t paddr_read(paddr_t addr, int len);

void paddr_write(paddr_t addr, int len, word_t data);

int load_img(const char *img_file, paddr_t address);

static inline word_t pmem_read(paddr_t addr, int len) {
  return host_read(guest_to_host(addr), len);
}

static inline void pmem_write(paddr_t addr, int len, word_t data) {
  host_write(guest_to_host(addr), len, data);
}

#ifdef CONFIG_DIFFTEST_STORE_COMMIT

#define STORE_QUEUE_SIZE 48
typedef struct {
    uint64_t addr;
    uint64_t data;
    uint8_t  mask;
    uint8_t  valid;
} store_commit_t;
extern store_commit_t store_commit_queue[STORE_QUEUE_SIZE];

void store_commit_queue_push(uint64_t addr, uint64_t data, int len);
store_commit_t *store_commit_queue_pop();
int check_store_commit(uint64_t *addr, uint64_t *data, uint8_t *mask);
#endif

#ifdef CONFIG_MULTICORE_DIFF
extern uint8_t* golden_pmem;

static inline word_t golden_pmem_read(paddr_t addr, int len) {
  assert(golden_pmem != NULL);
  void *p = &golden_pmem[addr - 0x80000000];
  switch (len) {
    case 1: return *(uint8_t  *)p;
    case 2: return *(uint16_t *)p;
    case 4: return *(uint32_t *)p;
    case 8: return *(uint64_t *)p;
    default: assert(0);
  }
}
#endif

#ifdef __cplusplus
}
#endif

#endif
