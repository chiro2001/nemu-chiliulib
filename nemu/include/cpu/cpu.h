#ifndef __CPU_CPU_H__
#define __CPU_CPU_H__

#include <common.h>
#include <setjmp.h>

extern jmp_buf exception_jbuf;

void exception(int ex_cause);
void cpu_exec(uint64_t n);

enum {
  SYS_STATE_UPDATE = 1,
  SYS_STATE_FLUSH_TCACHE = 2,
};
void set_sys_state_flag(int flag);
void mmu_tlb_flush(vaddr_t vaddr);

struct Decode;
void save_globals(struct Decode *s);
void fetch_decode(struct Decode *s, vaddr_t pc);

#endif
